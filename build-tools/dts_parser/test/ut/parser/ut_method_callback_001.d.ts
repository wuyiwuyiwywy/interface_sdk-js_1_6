/**
 * the ut for method in class
 *
 */
export class Test {
  func(callback: AsyncCallback<Want>): void;
}
