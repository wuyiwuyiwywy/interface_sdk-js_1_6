/**
 * the ut for jsdoc about typedef
 *
 * @typedef Test
 */
export interface Test {
  key: string;
  value: string;
}
