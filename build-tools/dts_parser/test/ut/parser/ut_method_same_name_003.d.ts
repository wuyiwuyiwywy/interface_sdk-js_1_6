/**
 * the ut for method in interface
 *
 */
export interface Test {
  func(str: string, str2: string): void;
  func(str: string): void;
}
