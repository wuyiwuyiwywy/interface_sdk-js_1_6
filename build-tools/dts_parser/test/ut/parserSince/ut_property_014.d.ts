/**
 * the ut for property in class, the property has permission,
 * and permission value includes two permissions
 */
export class test {
  /**
   * @permission ohos.permission.GET_SENSITIVE_PERMISSIONS or ohos.permission.GRANT_SENSITIVE_PERMISSIONS
   */
  name: string
}