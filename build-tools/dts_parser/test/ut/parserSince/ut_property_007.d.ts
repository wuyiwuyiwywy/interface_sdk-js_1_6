/**
 * the ut for property in interface, the property has permission,
 * and permission value includes two permissions
 */
export interface test {
  /**
   * @permission ohos.permission.GET_SENSITIVE_PERMISSIONS or ohos.permission.GRANT_SENSITIVE_PERMISSIONS
   */
  name: string
}