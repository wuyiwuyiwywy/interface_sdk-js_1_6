/**
 * the ut for property in namespace, the property has permission,
 * and permission value is null
 */
export namespace test {
  /**
   * @permission
   */
  const name: string
}