/**
 * the ut for property in class, the property has permission,
 * and permission value is not null
 */
export class test {
  /**
   * @permission ohos.permission.GRANT_SENSITIVE_PERMISSIONS
   */
  name: string
}