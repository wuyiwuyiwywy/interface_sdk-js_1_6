/**
 * the ut for method in interface, which has params and return value
 */
export interface Test {
  test(param1: string): number;
}