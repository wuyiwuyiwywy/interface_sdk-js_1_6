/**
 * the ut for parent node has since tag version, and some child nodes's version is null
 * 
 * @since 7
 */
export class Test {

  id: number;
  /**
   * @since 7
   */
  name: string;
  /**
   * @since 8
   */
  age: number;
}