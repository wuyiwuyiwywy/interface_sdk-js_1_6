/**
 * the ut for method in interface, which doesn't have params, but has return value
 */
export interface Test {
  test(): number;
}